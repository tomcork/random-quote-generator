import React from 'react';
import data from '../data/quotes';

const colours = ['#16a085', '#27ae60', '#2c3e50', '#f39c12', '#e74c3c', '#9b59b6', '#FB6964', '#342224', "#472E32", "#BDBB99", "#77B1A9", "#73A857"];

class QuoteBox extends React.Component {
    constructor (props) {
        super(props);
        var randomIndex = Math.floor(Math.random() * data.length);
        var randomColor = Math.floor(Math.random() * colours.length);

        document.body.style.backgroundColor = colours[randomColor];

        this.state = {
            text: data[randomIndex].quote,
            author: data[randomIndex].author,
            color: colours[randomColor]
        }

        this.displayNewQuote = this.displayNewQuote.bind(this);
    }

    displayNewQuote(newProps) {
        var randomIndex = Math.floor(Math.random() * data.length);
        var randomColor = Math.floor(Math.random() * colours.length);

        document.body.style.backgroundColor = colours[randomColor];

        this.setState({
            text : data[randomIndex].quote,
            author: data[randomIndex].author,
            color: colours[randomColor]
        });
    }

    render () {
        const twitterUrl = "https://twitter.com/intent/tweet?text=" + encodeURIComponent("\"" + this.state.text + "\" - " + this.state.author);
        return  (
            <div id="quote-box" >
                <h1 id="text" style={{color: this.state.color}}>"{this.state.text}"</h1>
                <h2 id="author" style={{color: this.state.color}}>- {this.state.author}</h2>
                <button id="new-quote" onClick={this.displayNewQuote}>New Quote</button>
                <a id="tweet-quote" href={twitterUrl}>Tweet Quote</a>
            </div>
        );
    }
}

export default QuoteBox;